'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var jadeData = require('./data.json');
var autoprefixer = require('autoprefixer');

var config = {
	src: {
		root: 'app',
		templates: 'app/templates',
		styles: 'app/styles',
		scripts: 'app/scripts',
		svgSprite: 'app/img/sprite/svg',
		pngSprite: 'app/img/sprite/raster',
	},
	dest: {
		root: 'dist',
		templates: 'dist',
		styles: 'dist/styles',
		scripts: 'dist/scripts',
		images: 'dist/img/theme',
	},
	jsConcat: [
		'./app/vendors/shave.min.js',
		'./app/vendors/lazyload.min.js',
	],
	browserSync: {
		reloadOnRestart: true,
		notify: false,
		port: 9000,
		startPath: "/",
		server: {
			baseDir: ['dist', 'app']
		}
	}
}

var jsDest = 'dist/scripts';

// compile jade
gulp.task('views', function() {
	return gulp.src([config.src.templates+'/**/*.jade'])
		.pipe($.plumber())

		//only pass unchanged *main* files and *all* the partials
		.pipe($.changed(config.dest.templates, { extension: '.html' }))

		//filter out unchanged partials, but it only works when watching
		.pipe($.if(browserSync.active, $.cached('jade')))

		//find files that depend on the files that have changed
		.pipe($.jadeInheritance({ basedir: config.src.templates }))

		//filter out partials (folders and files starting with "_" )
		.pipe($.filter(function(file) {
			return !/\_/.test(file.path) && !/^_/.test(file.relative);
		}))

		.pipe($.jade({
			locals: jadeData,
			pretty: false
		}))
		.pipe($.beml({
			elemPrefix: '__',
			modPrefix: '--',
			modDlmtr: '-'
		}))
		.pipe($.fileInclude({ basepath: config.dest.templates }))
		.pipe($.htmlPrettify({ indent_char: '	', indent_size: 1 }))
		.pipe(gulp.dest(config.dest.templates))
		.pipe(reload({ stream: true }));
});

// compile sass
gulp.task('styles', function() {
	var plugins = [
		autoprefixer({browsers: ['last 2 version']}),
	];
	$.rubySass(config.src.styles, {
			style: 'compressed', //compact, compressed, expanded
			precision: 10,
			sourcemap: true
		})
		.on('error', function(err) {
			console.error('Error!', err.message);
		})
		.pipe($.postcss(plugins))
		.pipe($.sourcemaps.write('.'))
		.pipe(gulp.dest(config.dest.styles))
		.pipe(reload({ stream: true }));
});

// view and check scripts
gulp.task('scripts', function() {
	return gulp.src([config.src.scripts+'/**/*.js'])
		.pipe($.filter(function(file) {
			return !/\_/.test(file.path) && !/^_/.test(file.relative);
		}))
		.pipe($.plumber())
		.pipe($.sourcemaps.init())
		.pipe($.babel({
			presets: ['es2015']
		}))
		.pipe($.sourcemaps.write('.'))
		.pipe(gulp.dest(config.dest.scripts));
});

// concat scripts
gulp.task('concat-scripts', function() {
	return gulp.src(config.jsConcat)
		.pipe($.concat('vendors.js'))
		.pipe(gulp.dest(config.dest.scripts))
});

// sprite-gen
gulp.task('png-sprite', function() {
	var spriteData = gulp.src(config.src.pngSprite+'/*.png').pipe($.spritesmith({
		imgName: 'sprite.png',
		cssName: '../../../app/styles/core/_sprite.scss',
		padding: 20,
		imgPath: '../img/theme/sprite.png'
	}));
	return spriteData.pipe(gulp.dest(config.dest.images));
});

// SVG sprite
gulp.task('svg-sprite', function() {
	gulp.src(config.src.svgSprite+'/*.svg')
		.pipe($.plumber())
		.pipe($.svgSprite({
			shape: {
				dimension: {
					maxWidth: 32,
					maxHeight: 32
				},
				spacing: {
					padding: 0
				},
				id: {
					generator: 'si-'
				}
			},
			mode: {
				symbol: {
					sprite: "../sprite.symbol.svg"
				}
			}
		})).on('error', function(error) { console.log(error); })
		.pipe(gulp.dest(config.dest.images));
});


// main task
gulp.task('serve', $.sync(gulp).sync([
	['views', 'png-sprite', 'styles', 'scripts', 'concat-scripts', 'svg-sprite']
]), function() {
	browserSync.init(config.browserSync);

	// watch for changes
	gulp.watch([
		config.dest.scripts+'/**/*.js',
		'app/img/**/*'
	]).on('change', reload);

	gulp.watch(config.src.scripts+'/**/*.js', ['scripts']);
	gulp.watch('app/vendors/**/*.js', ['concat-scripts']);
	gulp.watch(config.src.styles+'/**/*.scss', ['styles']);
	gulp.watch('app/**/*.jade', ['views']);
	gulp.watch(config.src.pngSprite+'/**/*.png', ['png-sprite']);
	gulp.watch(config.src.svgSprite+'/**/*.svg', ['svg-sprite', 'views']);
});

gulp.task('default', function() {
	gulp.start('serve');
});
