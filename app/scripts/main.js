const APP = {
	// сет брейкпоинтов для js
	// должны совпадать с теми что в body:after
	mediaBreakpoint: {
		sm: 576,
		md: 768,
		lg: 992,
		xl: 1200
	},
	cache: {},
	initBefore: function() {
		APP.polyfills()
		APP.svgIcons()
		document.documentElement.className =
			document.documentElement.className.replace("no-js", "js");
	},

	init: function() {
		APP.detectIE();
		APP.lazyload();

		APP.objectFitFallback(document.querySelectorAll('[data-object-fit]'));

		const myMask = new APP.Mask(".js-tel")
		myMask.init()

		APP.buttons();
		APP.closeOnFocusLost();
	},

	initOnLoad: function() {
		APP.truncateText( document.querySelectorAll('.js-dot') );
	},

	buttons: function() {
		Array.prototype.forEach.call(
			document.querySelectorAll('.menu-trigger'), function(item) {
			item.addEventListener('click', function() {
				document.body.classList.toggle('nav-showed')
			})
		})
	},

	closeOnFocusLost: function() {
		document.addEventListener('click', function(e) {
			const trg = e.target;
			if (!trg.closest(".header")) {
				document.body.classList.remove('nav-showed');
			}
		});
	},

	Mask: function(selector, regExpString = "+7 (___) ___-__-__") {
		this.elements = document.querySelectorAll(selector)
		this.init = function() {
			listeners(this.elements)
		}

		const listeners = (selector) => {
			for (let i = 0; i < selector.length; i++) {
				const input = selector[i];
				input.addEventListener("input", mask, false);
				input.addEventListener("focus", mask, false);
				input.addEventListener("blur", mask, false);
			}
		}

		const setCursorPosition = function(pos, elem) {
			elem.focus();
			if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
			else if (elem.createTextRange) {
				const range = elem.createTextRange();
				range.collapse(true);
				range.moveEnd("character", pos);
				range.moveStart("character", pos);
				range.select()
			}
		}


		const mask = function(event) {
			var matrix = regExpString,
				i = 0,
				def = matrix.replace(/\D/g, ""),
				val = this.value.replace(/\D/g, "");
			if (def.length >= val.length) val = def;
			this.value = matrix.replace(/./g, function(a) {
				return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
			});
			if (event.type == "blur") {
				if (this.value.length == 2) this.value = ""
			} else {
				setCursorPosition(this.value.length, this)
			}
		}

	},

	truncateText: function(selector) {
		const cutText = () => {
			for (let i = 0; i < selector.length; i++) {
				const text = selector[i]
				const elemMaxHeight = parseInt(getComputedStyle(text).maxHeight, 10)
				const elemHeight = text.offsetHeight
				const maxHeight = elemMaxHeight ? elemMaxHeight : elemHeight
				shave(text, maxHeight);
			}
		}

		APP.cache.cutTextListener = APP.throttle(cutText, 100)

		cutText();

		window.addEventListener('resize', APP.cache.cutTextListener);
	},

	lazyload: function() {
		if (typeof APP.myLazyLoad == 'undefined') {
			_regularInit()
		} else {
			_update()
		}

		function _update() {
			// console.log('LazyLoad update');
			APP.myLazyLoad.update();
			APP.objectFitFallback(document.querySelectorAll('[data-object-fit]'));
		}

		function _regularInit() {
			// console.log('LazyLoad first init');
			APP.myLazyLoad = new LazyLoad({
				elements_selector: ".lazyload",
				callback_error: function(el) {
					el.parentElement.classList.add('lazyload-error')
				}
			});
			APP.objectFitFallback(document.querySelectorAll('[data-object-fit]'));
		}
	},

	objectFitFallback: function(selector) {
		// if (true) {
		if ('objectFit' in document.documentElement.style === false) {
			for (var i = 0; i < selector.length; i++) {
				const that = selector[i]
				const imgUrl = that.getAttribute('src') ? that.getAttribute('src') : that.getAttribute('data-src');
				const dataFit = that.getAttribute('data-object-fit')
				let fitStyle
				if (dataFit === 'cover') {
					fitStyle = 'cover'
				} else {
					fitStyle = 'contain'
				}
				const parent = that.parentElement
				if (imgUrl) {
					parent.style.backgroundImage = 'url(' + imgUrl + ')'
					parent.classList.add('fit-img')
					parent.classList.add('fit-img--'+fitStyle)
				}
			};
		}
	},

	svgIcons: function () {
		const container = document.querySelector('[data-svg-path]')
		const path = container.getAttribute('data-svg-path')
		const xhr = new XMLHttpRequest()
		xhr.onload = function() {
			container.innerHTML = this.responseText
		}
		xhr.open('get', path, true)
		xhr.send()
	},

	polyfills: function () {
		/**
		 * polyfill for .closest
		 */
		(function(ELEMENT) {
			ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
			ELEMENT.closest = ELEMENT.closest || function closest(selector) {
				if (!this) return null;
				if (this.matches(selector)) return this;
				if (!this.parentElement) {return null}
					else return this.parentElement.closest(selector)
				};
		}(Element.prototype));

		Element.prototype.hasClass = function(className) {
			return this.className && new RegExp("(^|\\s)" + className + "(\\s|$)").test(this.className);
		};
	},

	detectIE: function() {
		/**
		 * detect IE
		 * returns version of IE or false, if browser is not Internet Explorer
		 */

		 (function detectIE() {
		 	var ua = window.navigator.userAgent;

		 	var msie = ua.indexOf('MSIE ');
		 	if (msie > 0) {
				// IE 10 or older => return version number
				var ieV = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
				document.querySelector('body').className += ' IE';
			}

			var trident = ua.indexOf('Trident/');
			if (trident > 0) {
				// IE 11 => return version number
				var rv = ua.indexOf('rv:');
				var ieV = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
				document.querySelector('body').className += ' IE';
			}

			var edge = ua.indexOf('Edge/');
			if (edge > 0) {
				// IE 12 (aka Edge) => return version number
				var ieV = parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
				document.querySelector('body').className += ' IE';
			}

			// other browser
			return false;
		})();
	},

	throttle: function (callback, limit) {
		var wait = false;
		return function() {
			if (!wait) {
				callback.call();
				wait = true;
				setTimeout(function() {
					wait = false;
				}, limit);
			}
		};
	},

	getScreenSize: function() {
		let screenSize =
			window
			.getComputedStyle(document.querySelector('body'), ':after')
			.getPropertyValue('content');
		screenSize = parseInt(screenSize.match(/\d+/));
		return screenSize;
	}
};

APP.initBefore();

document.addEventListener('DOMContentLoaded', function() {
	APP.init();
});

window.onload = function() {
	APP.initOnLoad();
};
